webpackJsonp([0],{

/***/ 101:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DatabaseProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_sqlite__ = __webpack_require__(195);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DatabaseProvider = /** @class */ (function () {
    function DatabaseProvider(sqlite) {
        this.sqlite = sqlite;
    }
    DatabaseProvider.prototype.getDB = function () {
        return this.sqlite.create({
            name: 'classficacao.db',
            location: 'default'
        });
    };
    DatabaseProvider.prototype.createDatabase = function () {
        var _this = this;
        return this.getDB()
            .then(function (db) {
            _this.createTables(db);
        })
            .catch(function (e) { return console.error(e); });
    };
    DatabaseProvider.prototype.createTables = function (db) {
        db.sqlBatch([
            ['CREATE TABLE IF NOT EXISTS usuarios (id integer primary key AUTOINCREMENT NOT NULL, nome TEXT, pontos integer, gp integer, gc integer, sg integer)']
        ])
            .then(function () { return console.log('Tabelas criadas'); })
            .catch(function (e) { return console.error('Erro ao criar as tabelas', e); });
    };
    DatabaseProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_native_sqlite__["a" /* SQLite */]])
    ], DatabaseProvider);
    return DatabaseProvider;
}());

//# sourceMappingURL=database.js.map

/***/ }),

/***/ 111:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 111;

/***/ }),

/***/ 152:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 152;

/***/ }),

/***/ 196:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__edit_usuario_edit_usuario__ = __webpack_require__(197);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_usuarios_usuarios__ = __webpack_require__(40);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var HomePage = /** @class */ (function () {
    function HomePage(toastCtrl, navCtrl, usuariosProvider) {
        this.toastCtrl = toastCtrl;
        this.navCtrl = navCtrl;
        this.usuariosProvider = usuariosProvider;
        this.usuarios = [];
    }
    HomePage.prototype.ionViewDidEnter = function () {
        this.listarTodos();
    };
    HomePage.prototype.addUsuarios = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__edit_usuario_edit_usuario__["a" /* EditUsuarioPage */]);
    };
    HomePage.prototype.listarTodos = function () {
        var _this = this;
        this.usuariosProvider.getAll()
            .then(function (result) {
            _this.usuarios = result;
            console.log(_this.usuarios);
        });
    };
    HomePage.prototype.excluirUsuario = function (usuario) {
        var _this = this;
        this.usuariosProvider.remove(usuario.id)
            .then(function () {
            var index = _this.usuarios.indexOf(usuario);
            _this.usuarios.splice(index, 1);
            _this.toastCtrl.create({
                message: 'Jogador removido',
                duration: 3000,
                position: 'bottom'
            }).present();
        });
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"C:\Users\alanm\ProjetosIonic\resultado\src\pages\home\home.html"*/'<ion-header>\n\n\n\n  <button ion-button menuToggle icon-only clear large color="primary">\n\n    <ion-icon name="menu"></ion-icon>\n\n  </button>\n\n\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n\n\n\n\n  <ion-list>\n\n    <ion-item-sliding *ngFor="let usuario of usuarios">\n\n\n\n      <button ion-item>\n\n        <h1>{{ usuario.nome }}</h1>\n\n      </button>\n\n\n\n      <ion-item-options side="left">\n\n\n\n        <button ion-button color="danger" (click)="excluirUsuario(usuario)">\n\n          <ion-icon name="trash"></ion-icon>\n\n          Excluir\n\n        </button>\n\n\n\n      </ion-item-options>\n\n\n\n    </ion-item-sliding>\n\n\n\n  </ion-list>\n\n\n\n  <ion-fab right bottom>\n\n    <button ion-fab color="light" (click)="addUsuarios()">\n\n      <ion-icon name="add"></ion-icon>\n\n    </button>\n\n  </ion-fab>\n\n</ion-content>'/*ion-inline-end:"C:\Users\alanm\ProjetosIonic\resultado\src\pages\home\home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_usuarios_usuarios__["b" /* UsuariosProvider */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 197:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EditUsuarioPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_usuarios_usuarios__ = __webpack_require__(40);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var EditUsuarioPage = /** @class */ (function () {
    function EditUsuarioPage(navCtrl, navParams, toast, usuariosProvider) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toast = toast;
        this.usuariosProvider = usuariosProvider;
        this.model = new __WEBPACK_IMPORTED_MODULE_2__providers_usuarios_usuarios__["a" /* Usuario */];
        if (this.navParams.data.id) {
            this.usuariosProvider.get(this.navParams.data.id)
                .then(function (result) {
                _this.model = result;
            });
        }
    }
    EditUsuarioPage.prototype.ionViewDidLoad = function () {
    };
    EditUsuarioPage.prototype.salvar = function () {
        var _this = this;
        this.salvarUsuario()
            .then(function () {
            _this.toast.create({
                message: 'Jogador cadastrado', duration: 3000, position: 'bottom'
            }).present();
        })
            .catch(function () {
            _this.toast.create({
                message: 'Erro ao salvar o jogador.', duration: 3000, position: 'bottom'
            }).present();
        });
    };
    EditUsuarioPage.prototype.salvarUsuario = function () {
        if (!this.model.id && this.model != null) {
            return this.usuariosProvider.insert(this.model);
        }
    };
    EditUsuarioPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-edit-usuario',template:/*ion-inline-start:"C:\Users\alanm\ProjetosIonic\resultado\src\pages\edit-usuario\edit-usuario.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>Jogadores</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n  <ion-list>\n\n\n\n    <ion-item>\n\n      <ion-label stacked>Nome</ion-label>\n\n      <ion-input type="text" name="nome" [(ngModel)]="model.nome"></ion-input>\n\n    </ion-item>\n\n\n\n  </ion-list>\n\n\n\n  <button ion-button block (click)="salvar()">Salvar</button>\n\n</ion-content>'/*ion-inline-end:"C:\Users\alanm\ProjetosIonic\resultado\src\pages\edit-usuario\edit-usuario.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_usuarios_usuarios__["b" /* UsuariosProvider */]])
    ], EditUsuarioPage);
    return EditUsuarioPage;
}());

//# sourceMappingURL=edit-usuario.js.map

/***/ }),

/***/ 198:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return QuemVaiJogarPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_usuarios_usuarios__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__hora_show_hora_show__ = __webpack_require__(199);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var QuemVaiJogarPage = /** @class */ (function () {
    function QuemVaiJogarPage(navCtrl, navParams, usuariosProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.usuariosProvider = usuariosProvider;
        this.usuarios = [];
        this.jogadores = [];
    }
    QuemVaiJogarPage.prototype.ionViewDidLoad = function () {
        this.listarTodos();
    };
    QuemVaiJogarPage.prototype.listarTodos = function () {
        var _this = this;
        this.usuariosProvider.getAll()
            .then(function (result) {
            _this.usuarios = result;
        });
    };
    QuemVaiJogarPage.prototype.selecionarJogadores = function (data, usuario) {
        if (data.checked == true) {
            this.jogadores.push(usuario);
        }
        else {
            var novoArray = this.jogadores.filter(function (el) {
                console.log(this.jogadores);
                return el.id !== usuario.id;
            });
            this.jogadores = novoArray;
        }
    };
    QuemVaiJogarPage.prototype.vamosJogar = function () {
        console.log(this.jogadores);
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__hora_show_hora_show__["a" /* HoraShowPage */], { Jogadores: this.jogadores });
        this.jogadores = null;
    };
    QuemVaiJogarPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-quem-vai-jogar',template:/*ion-inline-start:"C:\Users\alanm\ProjetosIonic\resultado\src\pages\quem-vai-jogar\quem-vai-jogar.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>Selecione quem vai jogar</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n  <ion-list>\n\n    <ion-item *ngFor="let usuario of usuarios">\n\n      <ion-label>{{ usuario.nome }}</ion-label>\n\n      <ion-checkbox color="dark" (click)="selecionarJogadores($event, usuario)"\n\n        (ionChange)="selecionarJogadores($event, usuario)" checked="false"></ion-checkbox>\n\n    </ion-item>\n\n  </ion-list>\n\n  <button ion-button full color="dark" (click)="vamosJogar()">\n\n    Vamos jogar!!! &nbsp;&nbsp;\n\n    <ion-icon name="football"></ion-icon>\n\n  </button>\n\n</ion-content>'/*ion-inline-end:"C:\Users\alanm\ProjetosIonic\resultado\src\pages\quem-vai-jogar\quem-vai-jogar.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_usuarios_usuarios__["b" /* UsuariosProvider */]])
    ], QuemVaiJogarPage);
    return QuemVaiJogarPage;
}());

//# sourceMappingURL=quem-vai-jogar.js.map

/***/ }),

/***/ 199:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HoraShowPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_usuarios_usuarios__ = __webpack_require__(40);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var HoraShowPage = /** @class */ (function () {
    function HoraShowPage(navCtrl, toastCtrl, usuarioProvider, navParams) {
        this.navCtrl = navCtrl;
        this.toastCtrl = toastCtrl;
        this.usuarioProvider = usuarioProvider;
        this.navParams = navParams;
        this.jogadores = this.navParams.get('Jogadores');
    }
    HoraShowPage.prototype.ionViewDidLoad = function () {
        console.log(this.jogadores);
    };
    HoraShowPage.prototype.finalizarPartida = function (play1, play2) {
        if (play1 > play2) {
            this.toastCtrl.create({
                message: 'Parabéns ' + this.jogadores[0].nome, duration: 3000, position: 'bottom'
            }).present();
            this.salvarResultado(this.jogadores[0]);
        }
    };
    HoraShowPage.prototype.salvarResultado = function (jogador) {
        console.log(jogador.id);
        if (jogador.gp == null) {
            jogador.gp = 5;
            this.usuarioProvider.update(jogador.id);
        }
        else {
            this.usuarioProvider.insert(jogador);
        }
    };
    HoraShowPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-hora-show',template:/*ion-inline-start:"C:\Users\alanm\ProjetosIonic\resultado\src\pages\hora-show\hora-show.html"*/'<ion-header>\n\n\n\n  <ion-title>Disputa</ion-title>\n\n\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n  <div *ngIf="jogadores">\n\n    <div class="scoreboard-outer">\n\n      <span class="team-name one">{{ jogadores[0].nome }}</span>\n\n      <div class="score-panel">\n\n        <span class="score team1">\n\n          <ion-input name="play1" [(ngModel)]="play1" class="text-input-play1" type="number"></ion-input>\n\n        </span><div class="barra">-</div>\n\n        <span class="score team2">\n\n          <ion-input name="play2" [(ngModel)]="play2" class="text-input-play2" type="number"></ion-input>\n\n        </span>\n\n      </div>\n\n      <span class="team-name two">{{ jogadores[1].nome }}</span>\n\n    </div>\n\n\n\n    \n\n  </div>\n\n  <button ion-button full class="btnFinalizar" (click)="finalizarPartida(play1, play2)">Finalizar partida</button>\n\n</ion-content>'/*ion-inline-end:"C:\Users\alanm\ProjetosIonic\resultado\src\pages\hora-show\hora-show.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_usuarios_usuarios__["b" /* UsuariosProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], HoraShowPage);
    return HoraShowPage;
}());

//# sourceMappingURL=hora-show.js.map

/***/ }),

/***/ 200:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(223);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 223:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(192);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(194);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_sqlite__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_component__ = __webpack_require__(273);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_home_home__ = __webpack_require__(196);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_database_database__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_usuarios_usuarios__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_edit_usuario_edit_usuario__ = __webpack_require__(197);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_quem_vai_jogar_quem_vai_jogar__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_hora_show_hora_show__ = __webpack_require__(199);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};













var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_7__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_edit_usuario_edit_usuario__["a" /* EditUsuarioPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_quem_vai_jogar_quem_vai_jogar__["a" /* QuemVaiJogarPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_hora_show_hora_show__["a" /* HoraShowPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */], {}, {
                    links: []
                })
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_7__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_edit_usuario_edit_usuario__["a" /* EditUsuarioPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_quem_vai_jogar_quem_vai_jogar__["a" /* QuemVaiJogarPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_hora_show_hora_show__["a" /* HoraShowPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_8__providers_database_database__["a" /* DatabaseProvider */],
                __WEBPACK_IMPORTED_MODULE_5__ionic_native_sqlite__["a" /* SQLite */],
                __WEBPACK_IMPORTED_MODULE_9__providers_usuarios_usuarios__["b" /* UsuariosProvider */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 273:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(194);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(192);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(196);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_database_database__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_quem_vai_jogar_quem_vai_jogar__ = __webpack_require__(198);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var MyApp = /** @class */ (function () {
    function MyApp(app, platform, statusBar, splashScreen, dbProvider) {
        this.app = app;
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.dbProvider = dbProvider;
        this.rootPage = null;
        this.initializeApp();
        this.pages = [
            { title: 'Home', component: __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */] },
            { title: 'Quem Vai Jogar', component: __WEBPACK_IMPORTED_MODULE_6__pages_quem_vai_jogar_quem_vai_jogar__["a" /* QuemVaiJogarPage */] },
        ];
    }
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
            _this.dbProvider.createDatabase()
                .then(function () {
                _this.openHomePage(_this.splashScreen);
            })
                .catch(function () {
                _this.openHomePage(_this.splashScreen);
            });
        });
    };
    MyApp.prototype.openHomePage = function (splashScreen) {
        splashScreen.hide();
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */];
    };
    MyApp.prototype.openPage = function (page) {
        this.nav.setRoot(page.component);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\Users\alanm\ProjetosIonic\resultado\src\app\app.html"*/'<ion-menu [content]="content" type="overlay">\n\n  <ion-header>\n\n    <ion-toolbar>\n\n      <ion-title>Menu</ion-title>\n\n    </ion-toolbar>\n\n  </ion-header>\n\n\n\n  <ion-content>\n\n    <ion-list>\n\n      <button menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)">\n\n        {{p.title}}\n\n      </button>\n\n    </ion-list>\n\n  </ion-content>\n\n\n\n</ion-menu>\n\n      \n\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>'/*ion-inline-end:"C:\Users\alanm\ProjetosIonic\resultado\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* App */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */], __WEBPACK_IMPORTED_MODULE_5__providers_database_database__["a" /* DatabaseProvider */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 40:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Usuario; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return UsuariosProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__database_database__ = __webpack_require__(101);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var Usuario = /** @class */ (function () {
    function Usuario() {
    }
    return Usuario;
}());

var UsuariosProvider = /** @class */ (function () {
    function UsuariosProvider(dbProvider) {
        this.dbProvider = dbProvider;
    }
    UsuariosProvider.prototype.insert = function (usuario) {
        return this.dbProvider.getDB()
            .then(function (db) {
            var sql = 'insert into usuarios (nome, pontos, gp, gc, sg) values (?, ?, ?, ?, ?)';
            var data = [usuario.nome, usuario.pontos, usuario.gp, usuario.gc, usuario.sg];
            return db.executeSql(sql, data);
        })
            .catch(function (e) { return console.log(e); });
    };
    UsuariosProvider.prototype.update = function (usuario) {
        return this.dbProvider.getDB()
            .then(function (db) {
            var sql = 'update usuarios set nome = ?, pontos = ?, gp = ?, gc = ?, sg = ? where id = ?';
            var data = [usuario.nome, usuario.pontos, usuario.gp, usuario.gc, usuario.sg];
            return db.executeSql(sql, data);
        })
            .catch(function (e) { return console.log(e); });
    };
    UsuariosProvider.prototype.remove = function (id) {
        return this.dbProvider.getDB()
            .then(function (db) {
            var sql = 'delete from usuarios where id = ?';
            var data = [id];
            return db.executeSql(sql, data);
        })
            .catch(function (e) { return console.log(e); });
    };
    UsuariosProvider.prototype.get = function (id) {
        return this.dbProvider.getDB()
            .then(function (db) {
            var sql = 'select * from usuarios where id = ?';
            var data = [id];
            return db.executeSql(sql, data)
                .then(function (data) {
                if (data.rows.length > 0) {
                    var item = data.rows.item(0);
                    var usuario = new Usuario();
                    usuario.id = item.id;
                    usuario.nome = item.nome;
                    usuario.pontos = item.pontos;
                    usuario.gp = item.gp;
                    usuario.gc = item.gc;
                    usuario.sg = item.sg;
                    return usuario;
                }
                return null;
            })
                .catch(function (e) { return console.error(e); });
        })
            .catch(function (e) { return console.error(e); });
    };
    UsuariosProvider.prototype.getAll = function (active, name) {
        var _this = this;
        return this.dbProvider.getDB()
            .then(function (db) {
            var sql = 'SELECT * from usuarios';
            return db.executeSql(sql, _this.dados)
                .then(function (data) {
                if (data.rows.length > 0) {
                    var usuarios = [];
                    for (var i = 0; i < data.rows.length; i++) {
                        var user = data.rows.item(i);
                        usuarios.push(user);
                    }
                    return usuarios;
                }
                else {
                    return [];
                }
            })
                .catch(function (e) { return console.error(e); });
        })
            .catch(function (e) { return console.error(e); });
    };
    UsuariosProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__database_database__["a" /* DatabaseProvider */]])
    ], UsuariosProvider);
    return UsuariosProvider;
}());

//# sourceMappingURL=usuarios.js.map

/***/ })

},[200]);
//# sourceMappingURL=main.js.map